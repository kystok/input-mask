let Mask = require('../index'),
    assert = require('chai').assert;

describe('Mask ДД.ММ.ГГГГ', () => {
    it('Вывод пустой маски', () => {
        let myMask = new Mask(['0 - 9'], 'ДД.ММ.ГГГГ', '.', 10);
        assert.equal(myMask.getBasicMask(), 'ДД.ММ.ГГГГ');
    });
    it('Добавление 0 символов', () => {
        let myMask = new Mask(['0 - 9'], 'ДД.ММ.ГГГГ', '.', 10);
        let res = myMask.add();
        assert.equal(res, 'ДД.ММ.ГГГГ');
    });
    it('Добавление 1 символа', () => {
        let myMask = new Mask(['0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        let res = myMask.add(1);
        assert.equal(res, '1Д.ММ.ГГГГ');
    });
    it('Добавление всех символов', () => {
        let myMask = new Mask(['0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(1);
        myMask.add(2);
        myMask.add(0);
        myMask.add(3);
        myMask.add(1);
        myMask.add(9);
        myMask.add(9);
        myMask.add(6);
        let res = myMask.getResultMask();
        assert.equal(res, '12.03.1996');
    });
    it('Удаление символа', () => {
        let myMask = new Mask(['0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(1);
        myMask.add(2);
        myMask.add(0);
        myMask.add(3);
        myMask.add(1);
        myMask.add(9);
        myMask.add(9);
        myMask.add(6);
        myMask.del(7);
        let res = myMask.getResultMask();
        assert.equal(res, '1Д.ММ.ГГГГ');
    });

    it('Удаление символа', () => {
        let myMask = new Mask(['0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(1);
        myMask.add(2);
        myMask.add(0);
        myMask.clear();
        let res = myMask.getResultMask();
        assert.equal(res, 'ДД.ММ.ГГГГ');
    });

    it('Добавление 4 символов за раз', () => {
        let myMask = new Mask(['0-9'], 'ДД.ММ.ГГГГ', '.', 10);

        myMask.add([1,2,0,3]);
        let res = myMask.getResultMask();
        assert.equal(res, '12.03.ГГГГ');
    });
    it('Повторение маски', () => {
        let myMask = new Mask(['0-9'], 'ДД.', '.',5);
        let res = myMask.getResultMask();
        assert.equal(res, 'ДД.ДД');
    });
    it('Добавление сисмволов в повтореную маски', () => {
        let myMask = new Mask(['0-9'], 'ДД.', '.',4);
        myMask.add([1,2,0,3]);
        let res = myMask.getResultMask();
        assert.equal(res, '12.03');
    });
    it('Фильтр входных данных - доспустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(0);
        assert.equal(myMask.getResultMask(), '0Д.ММ.ГГГГ');
    });
    it('Фильтр входных данных - не допустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(4);
        assert.equal(myMask.getResultMask(), 'ДД.ММ.ГГГГ');
    });
    it('Массовый фильтр входных данных - доспустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(1);
        myMask.add(2);
        myMask.add(0);
        myMask.add(3);
        assert.equal(myMask.getResultMask(), '12.03.ГГГГ');
    });
    it('Массовый фильтр входных данных - не допустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add(1);
        myMask.add(2);
        myMask.add(2);
        myMask.add(4);
        assert.equal(myMask.getResultMask(), '12.ММ.ГГГГ');
    });
    it('Массовый (входные данные в массиве) фильтр входных данных - доспустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add([1,2,0,3,1,9,9,6]);
        assert.equal(myMask.getResultMask(), '12.03.1996');
    });
    it('Массовый (входные данные в массиве) фильтр входных данных - не допустимое значение', () => {
        let myMask = new Mask(['0-3', '0-9', '0-1', '0-9','1-2', '0-9', '0-9','0-9'], 'ДД.ММ.ГГГГ', '.', 10);
        myMask.add([1,2,0,3,0,9,9,6]);
        assert.equal(myMask.getResultMask(), '12.03.ГГГГ');
    });
});

describe('Mask a-zA-Zа-яA-Я', ()=>{
    it('Вывод буквенной маски', () => {
        let myMask = new Mask(['a-z'], '****', '');
        myMask.add(['a','b','c','d']);
        assert.equal(myMask.getResultMask(), 'abcd');
    });
    it('Вывод буквенной маски', () => {
        let myMask = new Mask(['a-zA-Z'], '****', '');
        myMask.add(['a','B','c','d']);
        assert.equal(myMask.getResultMask(), 'aBcd');
    });
    it('Ошибка фильтрации маски', () => {
        let myMask = new Mask(['a-z'], '****', '');
        myMask.add(['a','B','c','d']);
        assert.equal(myMask.getResultMask(), 'acd*');
    });
});

describe('Static simbols in mask', ()=>{
    it('Телефон', () => {
        let myMask = new Mask(['0-9'], '+7(___)_', '+7()-');
        myMask.add(['9','8','1','7']);
        assert.equal(myMask.getResultMask(), '+7(981)7');
    });

    it('Удаление номера', () => {
        let myMask = new Mask(['0-9'], '+7(___)___-__-__', '+7()-');
        myMask.add(['9','8','1','7','3','7','6','4','8','7']);
        myMask.del(9);
        assert.equal(myMask.getResultMask(), '+7(9__)___-__-__');
    });
});