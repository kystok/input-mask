"use strict";

module.exports = class Mask {
    constructor(array = '0-9a-zA-Zа-яА-Я', mask = '', delimiter = '', count = -1) {
        this.basicMask = mask;
        this.regexpDeliniter = new RegExp(`[${delimiter}]`);
        this.mask = mask;
        while(this.mask.length<count) {
            if ((count-this.mask.length)>this.mask.length) {
                this.mask +=mask
            } else {
                let tmp = this.mask.split("");
                for (let i =0; i<(count-this.mask.length); i++){
                    this.mask += tmp[i];
                }
            }
        }
        this.delimiter = delimiter;
        this.delimiterArr = [];
        this.delimiterCount = 0;
        this.setDelimiterArr();
        this.count = (count === -1) ? mask.length : count;
        this.array = array;
        this.result = this.mask;
        this.position = 0;
    }
    setDelimiterArr(){
        let tmp = this.mask.split("");
        tmp.map((l,i)=>{
            if (this.regexpDeliniter.exec(l)) this.delimiterArr.push(i)
        });
    }

    getBasicMask() {
        return this.basicMask;
    }
    getRealMask(){
        return this.mask;
    }
    getResultMask() {
        return this.result;
    }

    clear(){
        this.result = this.basicMask;
        return this.result;
    }
    del(count=1){
        if(this.position<1) {this.position=0;return this.result};
        let tmp = this.result.split(""),
            tmp2 = this.basicMask.split("");
        for (let i=0; i<count; i++){
            this.position--;
            while(this.delimiterArr.indexOf(this.position)!==-1){
                this.position--;
            }
            tmp[this.position] = tmp2[this.position];
        }
        this.result =  tmp.join("");
        return this.result;
    }
    add(letter = '') {
        if (letter === '') return this.result;
        if (this.position+letter.length>this.count) return this.result;
        let letterTMP, tmp,
            _array = "",_i=0;
        if (typeof letter === 'number' || typeof letter === 'string')
            if (letter.length > 1) {
                letterTMP = [letter.substring(0, 1)]
            } else {
                letterTMP = [letter];
            }
        if (typeof letter === 'object') {
            if ('map' in letter) {
                letterTMP = letter;
            } else {
                throw new Error('Input data can not be an object');
            }
        }
        tmp = this.result.split("");
        letterTMP.map((l,i)=>{
            while (this.delimiterArr.indexOf(this.position)!==-1) {
                this.position++;
                this.delimiterCount++;
            }
            let t = (this.position===0)? 0 : (this.position-this.delimiterCount)%this.array.length;
            _array = (t===-1) ? this.array[this.array.length-1] : this.array[t];
            let regexp = new RegExp(`[${_array}]`,'g');
            if (regexp.exec(l) === null) return;
            tmp[this.position] = l;
            this.position++;
        });
        this.result = tmp.join("");
        return this.result;
    }
};
